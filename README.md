# Exploring helpers for mods
(aka `dijkstra-mod` aka `how`)

This project: <https://gitlab.com/awgrover/mods-exploratorium>

live `mods` (treat as demo only): <http://mods.cba.mit.edu/>

code for mods: <https://gitlab.fabcloud.org/pub/project>

Original Question: How do I hook these things together to get from A to B?
E.g. I have a SVG and I want to loft it and make a STL for 3D printing.

## Strategy
Using the self-describing information in a `mod`, like

    # modules/image/size 
    var inputs = {
        image:{type:'RGBA'

calculate hooking some together.

A `mod` has inputs and outputs, and they have names and sometimes they have types, like:

    INPUT        TYPE
    -----        ----
    image        RGBA
    trigger      event

So, just do a route from the first input type to the last, like GPS navigation. In fact, use the basic routing algorithm ("Dijkstra").

Using raku (the language formerly known as perl6):

* find all the mods (from a local installation)
* ask each mod for the inputs/outputs by wrapping each in a little javascript
    * but also the names of the inputs/outputs which often reflect the actual or more specific type
* accumulate all that info for all the mods
* turn it into a list of each input->output
* ask Dijkstra to do a shortest-route path
* print it out in text

## Discussion

I showed the tool around to some cba students, and was surprised at the enthusiasm. The tool is very basic, just barely works, and makes no claim that the results are meaningful.

    # How do you get from `string` to `SVG`?
    % ./how  path  string SVG
    string -> object
       modules/convert/svg/image, convert SVG image
       modules/object/string, object string
    object -> file
       modules/image/raster mask, raster mask
       modules/image/vector mask, vector mask
    file -> path
      WAT?
    path -> SVG
       modules/image/vector mask, vector mask
       modules/path/formats/svg, path to SVG

Even when present, the types are not specific enough to give a good "route". For example, `text` meant for a filename.
The names of the inputs and outputs often carry the additional semantic information, e.g. `filename`.

### Other features

It also has `search` and `list` all.

### Obvious improvements

* generate a mods "program"
* rewrite in node-js -- not hard

### More ideas

* add to `mods`. Seems pretty obvious once said.
* kind of like "autocomplete", suggest more of the network based on the mods you have so far: "ghost" mods.
* context search/help at an input and output.

## Usage

    Usage:
      ./how [--types=<Any>] [--debug=<Any>] [--limit=<Any>] [--show-js=<Any>] list -- list local mods
      ./how [--debug=<Any>] [--limit=<Any>] [--show-js=<Any>] test -- initial/explore dev code
      ./how [--debug=<Any>] [--limit=<Any>] [--show-js=<Any>] search <word> -- Find modules w/ in|out|name matching
      ./how [--debug=<Any>] [--limit=<Any>] [--show-js=<Any>] path <from> <to> -- Suggest a sequence of mods $from->$to (types). Try --types 'list'

    % ./how  path  string SVG
    string -> object
       modules/convert/svg/image, convert SVG image
       modules/object/string, object string
    object -> file
       modules/image/raster mask, raster mask
       modules/image/vector mask, vector mask
    file -> path
      WAT?
    path -> SVG
       modules/image/vector mask, vector mask
       modules/path/formats/svg, path to SVG

    % ./how search RGBA
    ! vector mask  {inputs => {image => RGBA, imageInfo => object, palette => text, path => array}, outputs => {SVG => file, color => RGB, image => RGBA}, path => modules/image/vector mask}
    ! convert RGBA to PNG  {inputs => {image => RGBA, imageInfo => object}, outputs => {}, path => modules/convert/rgba/png}
    ! mesh slice raster  {inputs => {mesh => STL, settings => (Any)}, outputs => {image => RGBA, imageInfo => (Any)}, path => modules/mesh/slice raster}
    ! frep render (CPU)  {inputs => {shape => frep}, outputs => {image => RGBA}, path => modules/frep/view/render (CPU)}
    (and lots more...)

## Dependencies

* raku
* node js
* local install of mods
* `ln -s $nodesInstallDir mods`

# the footnotes

* Raku is lower cognitive impedance mismatch for me.
* APA title capitalization is ... pedantic.
